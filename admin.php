<?php
include('./lib.php');
$db = new DB('data.db');
$cl = new Client();
?>

<?php
echo $cl->createForm("createTP", "Создать таблицу участников");
echo $cl->createForm("listP", "Перечислить участников");
echo $cl->createForm("checkT", "Проверить наличие таблицы");
echo $cl->createForm("startL", "Начать лотерею");
echo $cl->createForm("outputDraws", "Вывести розыгрыши");
echo $cl->createForm("regPartic", "Зарегистрировать участника");
$proc = $_POST['proc'];
switch ($proc){
    case 'createTP':
        $db->createParticipants();
        break;
    case 'listP':
        $db->listParticipants();
        break;
    case 'checkT':
        echo $db->checkTable('participants');
        break;
    case 'startL':
        // $db->createTable('drawings', 'datefrom, dateto');
        $db->startLottery();
        break;
    case 'outputDraws':
        echo 'Начало вывода.';
        echo $db->outputTable('drawings');
        break;
    case 'regPartic':
        $p = new Participant();
        // echo $p->regParticipant($db);
        // echo $db->addParticipant();
        $p->won = $p->compete($db);
        echo $cl->message('message', "id нового участника: " 
        . $db->addParticipant($p->won));
        echo $cl->message('message', "Результат: {$p->won}");
        break;
}
?>