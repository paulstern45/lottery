<?php
include('./lib.php');
$db = new DB('data.db');
$p = new Participant();
$cl = new Client();
$p->dtime = new DateTime('now');
$p->won = $p->compete($db);
$p->id = $db->addParticipant($p->won);
?>
<div>
    <div>Ваш лотерейный номер: 
        <div class="id">
            <?php echo $p->id . PHP_EOL;?>
        </div>
    </div>
    <div>Дата регистрации:
        <div class="dtime">
            <?php echo $p->dtime->format('r') . PHP_EOL;?>
        </div>
    </div>
    <?php $cl->result($p->won); ?>
</div>
