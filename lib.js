const htmlEl = function (tagname, clName, content) {
    let el = document.createElement(tagname);
    el.className = clName;
    el.innerHTML = content;
    return el;
}
const appendEl = function (el) {
    document.body.append(el);
}
const countDown = function (seconds, el) {
    if (seconds-- <= 1) {
        el.innerHTML = seconds;
    } else {
        el.innerHTML = seconds;
        setTimeout(countDown, 1000, seconds, el);
    }
}
const timer = function (sec) {
    const toSec = function (h, m, s) {
        return h * 3600 + m * 60 + s;
    }
    const toTime = function (sec) {
        let s = sec;
        let h = Math.floor(s / 3600);
        let m = Math.floor((s % 3600) / 60);
        s = s % 60;

        return [h, m, s];
    }
    const countDown = function (seconds, el) {
        if (seconds-- <= 1) {
            let [h, m, s] = toTime(seconds);
            el.innerHTML = `${h}:${m}:${s}`;
            return;
        } else {
            let [h, m, s] = toTime(seconds);
            el.innerHTML = `${h}:${m}:${s}`;
            setTimeout(countDown, 1000, seconds, el);
        }
    }

    const [h, m, s] = toTime(sec);
    let t = htmlEl('div', 'timer', `${h}:${m}:${s}`);
    appendEl(t);
    countDown(sec, t);
    return;
}