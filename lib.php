<?php
class DB {
    public $db;
    function __construct($file_name){
        $this->db = new SQLite3("$file_name");
    }
    function createTable($t_name, $sql_str){
        $this->db->exec(sprintf('CREATE TABLE %s(%s);', $t_name, $sql_str));
    }
    function checkTable($t_name){
        return $this->db->exec("SELECT name FROM sqlite_master WHERE type='table' AND name='{$t_name}'");
    }
    function query($sql_str){
        return $this->db->exec("sql_str");
    }
    function addIntoTable($t_name, $t_fields, $sql_str){
        $this->db->exec(sprintf('INSERT INTO %s(%s) VALUES %s', $t_name, $t_fields, $sql_str));
    }
    function addParticipant($won){
        $this->db->exec(<<<EOT
        INSERT INTO participants(dtime, won)
        VALUES (datetime('now'), "$won");
        SELECT last_insert_rowid();
        EOT);
        return $this->db->lastInsertRowID();
    }
    function outputTable($t_name) {
        return $this->db->query("SELECT * from $t_name");
    }
    function createParticipants(){
        $this->db->exec(<<<EOT
            CREATE TABLE participants (
                id INTEGER NOT NULL UNIQUE,
                dtime INTEGER,
                won TEXT,
                PRIMARY KEY("id" AUTOINCREMENT)
            );
        EOT);
    }
    function listParticipants(){
        return $this->outputTable('participants');
    }
    function startLottery(){
        $this->createParticipants();
        $from = new DateTime('now');
        $format = '';
        $this->db->exec(<<<EOD
            CREATE TABLE drawings (
                date_from INTEGER,
                date_to INTEGER
            );
            INSERT INTO drawings(date_from, date_to)
            VALUES
            (datetime('now'), datetime('now', '+1 day')),
            (datetime('now', '+1 day'), datetime('now', '+2 day')),
            (datetime('now', '+2 day'), datetime('now', '+3 day')),
            (datetime('now', '+3 day'), datetime('now', '+4 day')),
            (datetime('now', '+4 day'), datetime('now', '+5 day')),
            (datetime('now', '+5 day'), datetime('now', '+6 day')),
            (datetime('now', '+6 day'), datetime('now', '+7 day'));
            EOD);
    }
    function countRows($table) {
        return $this->db->query("SELECT COUNT(*) FROM $table")->fetchArray(SQLITE3_NUM)[0];
    }
    function countDrawings() {
        return $this->countRows('drawings');
    }
    function countParticipants() {
        return $this->countRows('participants');
    }
    function getTable($table) {
        $result = $this->db->query("SELECT * FROM $table");
        $arr = array();
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($arr, $row);
        }
        return $arr;
    }
    function getParticipants() {
        return $this->getTable('participants');
    }
    function countWinners($arr) {
        $count = 0;
        foreach($arr as $row) {
            if(strpos($row['won'], 'T') !== false) {
                $count++;
            }
        }
        return $count;
    }
    function getResults() {
        $result = $this->db->query('SELECT won FROM participants;');
        $arr = array();
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($arr, $row);
        }
        return $arr;
    }
    function convertToDT($date) {
        return new DateTime("$date", new DateTimeZone('UTC'));
    }
    function countCurrentDrawings($dtime) {
        $count = 0;
        $dr = $this->getTable('drawings');
        $start = $this->convertToDT($dr[0]['date_from']);
        $end = $this->convertToDT($dr[count($dr) - 1]['date_to']);
        if ($dtime < $start || $end < $dtime) {
            return $count;
        }
        foreach($dr as $row) {
            $df = $this->convertToDT($row['date_from']);
            $dt = $this->convertToDT($row['date_to']);
            if ($df < $dtime) {
                $count++;
            }
        }
        return $count;
    }

}
class Participant {
    public $id;
    public $dtime;
    public $won;
    function __construct(){
        $this->dtime = new DateTime('now');
    }
    function draw(){
        return random_int(1, 20) == 20;
    }
    function regParticipant($db){
        $this->won = false;
        $db->db->exec(<<<EOT
            INSERT INTO participants(dtime, won)
            VALUES (datetime('now'), 'F');
        EOT);
        return $this->won;
    }
    function compete($db){
        $result;
        $now = new DateTime('now');
        $draws = $db->countCurrentDrawings($now);
        $diff = $draws - $db->countWinners($db->getTable('participants'));
        switch ($diff):
            case 6:
                $result = $this->draw();
                if ($result) break; 
            case 5:
                $result = $this->draw();
                if ($result) break; 
            case 4:
                $result = $this->draw();
                if ($result) break; 
            case 3:
                $result = $this->draw();
                if ($result) break; 
            case 2:
                $result = $this->draw();
                if ($result) break; 
            case 1:
                $result = $this->draw();
                if ($result) break; 
            default:
                return 'F';
        endswitch;
        if ($result) {
            return 'T';
        } else {
            return 'F';
        }
    }
}
class Client 
{
    function createForm($proc_name, $sub_val){
        return <<<EOT
                <form action="" method="POST">
                    <input type="hidden" name="proc" value="$proc_name">
                    <input type="submit" value="$sub_val">
                </form>
        EOT;
    }
    function message($class, $mes){
        return <<<EOT
            <div="$class">
                $mes
            </div>
        EOT;
    }
    function win(){
        return $this->message('result', <<<EOD
            Фортуна Вам улыбнулась! Вы выиграли 1 млн турецких лир!
        EOD);
    }
    function lose(){
        return $this->message('result', <<<EOD
            Эх, Вы проиграли. Попробуйте еще, в следующий раз
            обязательно повезет!
        EOD);
    }
    function result($r){
        switch($r):
            case 'T':
                echo $this->win();
                break;
            case 'F':
                echo $this->lose();
                break;
            endswitch;
        return;
    }
}
?>